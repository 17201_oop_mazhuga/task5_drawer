//
// Created by d.mazhuga on 19.11.2018.
//

#ifndef INC_05_DRAWER_NAMED_H
#define INC_05_DRAWER_NAMED_H

#include <map>

class Named {
private:
    static std::map <std::string, unsigned> namedMap;
    static std::map <std::string, unsigned> createMap();
protected:
    std::string name;
    static std::string generateName(std::string);
public:
    Named() = default;
    virtual ~Named(){};
    explicit Named(std::string name);
    std::string getName() const;
    friend std::ostream& operator<<(std::ostream &out, const Named &nmd);
};


#endif //INC_05_DRAWER_NAMED_H
