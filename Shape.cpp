//
// Created by d.mazhuga on 19.11.2018.
//

#include "Shape.h"

void Shape::setCentralCoordinates(unsigned x0, unsigned y0) {
    xCenter = x0;
    yCenter = y0;
}

bool Shape::operator==(const Shape &other) const {
    if (typeid(*this) != typeid(other) || xCenter != other.xCenter || yCenter != other.yCenter)
        return false;

    return equal(other);
}

Rectangle::Rectangle(unsigned width, unsigned height, unsigned xc, unsigned yc, Color clr) : width(width),
                                                                                             height(height) {
    xCenter = xc;
    yCenter = yc;
    color = clr;
    name = Named::generateName("Rectangle");
}

std::vector<Segment> Rectangle::getSegments() const {
    std::vector<Segment> retVector;

    //top line
    retVector.emplace_back(xCenter - (width / 2), yCenter + (height / 2), xCenter + (width / 2),
                           yCenter + (height / 2));
    //right line
    retVector.emplace_back(xCenter + (width / 2), yCenter + (height / 2), xCenter + (width / 2),
                           yCenter - (height / 2));
    //bottom line
    retVector.emplace_back(xCenter - (width / 2), yCenter - (height / 2), xCenter + (width / 2),
                           yCenter - (height / 2));
    //left line
    retVector.emplace_back(xCenter - (width / 2), yCenter - (height / 2), xCenter - (width / 2),
                           yCenter + (height / 2));

    return retVector;
}

double Rectangle::square() const {
    return width * height;
}

void Rectangle::setParameters(unsigned w, unsigned h) {
    width = w;
    height = h;
}

bool Rectangle::equal(const Shape &other) const {
    const auto &otherRect = dynamic_cast<const Rectangle&>(other);
    return width == otherRect.width && height == otherRect.height;
}

std::vector<Segment> Triangle::getSegments() const {
    std::vector<Segment> retVector;

    retVector.emplace_back(xCenter, yCenter, xCenter + x1, yCenter + y1);
    retVector.emplace_back(xCenter, yCenter, xCenter + x2, yCenter + y2);
    retVector.emplace_back(xCenter + x2, yCenter + y2, xCenter + x1, yCenter + y1);

    return retVector;
}

Triangle::Triangle(unsigned x1, unsigned y1, unsigned x2, unsigned y2, unsigned xc, unsigned yc,
                   Color clr) : x1(x1), y1(y1), x2(x2), y2(y2) {
    xCenter = xc;
    yCenter = yc;
    color = clr;
    name = Named::generateName("Triangle");
}

double Triangle::square() const {
    //Heron's formula
    double a, b, c, p, s;

    a = sqrt((xCenter - x1)*(xCenter - x1) + (yCenter - y1)*(yCenter - y1));
    b = sqrt((xCenter - x2)*(xCenter - x2) + (yCenter - y2)*(yCenter - y2));
    c = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

    p = (a + b + c) / 2;

    s = sqrt(p * (p - a) * (p - b) * (p - c));

    return s;
}

void Triangle::setCoordinates(unsigned x1_a, unsigned y1_a, unsigned int x2_a, unsigned int y2_a) {
    x1 = x1_a;
    y1 = y1_a;
    x2 = x2_a;
    y2 = y2_a;
}

bool Triangle::equal(const Shape &other) const {
    const auto &otherTr = dynamic_cast<const Triangle&>(other);
    return x1 == otherTr.x1 && y1 == otherTr.y1 && x2 == otherTr.x2 && y2 == otherTr.y2;
}

Circle::Circle(unsigned radius, unsigned xc, unsigned yc, Color clr) : radius(radius) {
    xCenter = xc;
    yCenter = yc;
    color = clr;
    name = Named::generateName("Circle");
}

std::vector<Segment> Circle::getSegments() const {
    std::vector<Segment> retVector;

    unsigned x, y, xPrev, yPrev;
    double i = 1;
    xPrev = x = radius;
    yPrev = y = static_cast<unsigned>(sqrt(radius * radius - xPrev * xPrev));

    while (x > 0) {
        i -= 0.05;

        //x^2 + y^2 = radius^2
        //calculates 1/4 of circle with (0,0) center

        x = static_cast<unsigned>(xPrev * i);
        y = static_cast<unsigned>(sqrt(radius * radius - x * x));

        retVector.emplace_back(xCenter + xPrev, yCenter + yPrev, xCenter + x, yCenter + y);
        retVector.emplace_back(xCenter - xPrev, yCenter + yPrev, xCenter - x, yCenter + y);
        retVector.emplace_back(xCenter + xPrev, yCenter - yPrev, xCenter + x, yCenter - y);
        retVector.emplace_back(xCenter - xPrev, yCenter - yPrev, xCenter - x, yCenter - y);

        xPrev = x;
        yPrev = y;
    }

    return retVector;
}

double Circle::square() const {
    return 2 * M_PI * radius * radius;
}

void Circle::setRadius(unsigned rad) {
    radius = rad;
}

bool Circle::equal(const Shape &other) const  {
    const auto &otherCir = dynamic_cast<const Circle&>(other);
    return radius == otherCir.radius;
}
