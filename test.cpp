#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "catch.hpp"
#include <algorithm>

#include "Shape.h"
#include "Drawer.h"
#include "parse.hpp"

TEST_CASE("Named name generation") {
    Named n1("A"), n2("A"), n3("B");

    REQUIRE(n1.getName() == "A.1");
    REQUIRE(n2.getName() == "A.2");
    REQUIRE(n3.getName() == "B.1");
}

TEST_CASE("Squares") {
    unsigned x1 = 100, y1 = 250, x2 = 0, y2 = 300;

    class Rectangle R(x1, y1);
    Triangle T(x1, y1, x2, y2);
    Circle C(x1);

    double rs, cs, ts;
    rs = x1 * y1;
    cs = 2 * M_PI * x1 * x1;

    double a, b, c, p;
    a = sqrt(x1 * x1 + y1 * y1);
    b = sqrt(x2 * x2 + y2 * y2);
    c = sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    p = (a + b + c) / 2;

    ts = sqrt(p * (p - a) * (p - b) * (p - c));

    REQUIRE(R.square() == rs);
    REQUIRE(T.square() == ts);
    REQUIRE(C.square() == cs);
}

TEST_CASE("totalArea function") {
    unsigned x1 = 100, y1 = 250, x2 = 0, y2 = 300;

    class Rectangle R(x1, y1);
    Triangle T(x1, y1, x2, y2);
    Circle C(x1);

    REQUIRE(totalArea(R, T, C) == R.square() + T.square() + C.square());
}

TEST_CASE("Segments for rectangle and triangle") {
    class Rectangle r(100, 200, 50, 100);
    Triangle t(0, 100, 200, 0);
    std::vector<Segment> seg;

    seg = r.getSegments();
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(0, 0, 100, 0)) != std::end(seg));
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(0, 0, 0, 200)) != std::end(seg));
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(100, 200, 100, 0)) != std::end(seg));
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(100, 200, 0, 200)) != std::end(seg));

    seg = t.getSegments();
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(0, 0, 0, 100)) != std::end(seg));
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(0, 0, 200, 0)) != std::end(seg));
    REQUIRE(std::find(std::begin(seg), std::end(seg), Segment(0, 100, 200, 0)) != std::end(seg));
}

TEST_CASE("Parsing canvas size", "Parser") {
    unsigned x = 127659, y = 9763290;
    std::stringstream sstream(std::to_string(x) + 'x' + std::to_string(y));

    std::pair<unsigned, unsigned> canvasSize = parseCanvasSize(sstream);
    REQUIRE(canvasSize.first == x);
    REQUIRE(canvasSize.second == y);
}

TEST_CASE("Parsing color", "Parser") {
    std::stringstream ss1("{black}"),
            ss2("{128, 64, 32}"),
            ss3("aaaaa");

    Color c1 = parseColor(ss1);
    REQUIRE(c1.getRed() == 0);
    REQUIRE(c1.getGreen() == 0);
    REQUIRE(c1.getBlue() == 0);

    Color c2 = parseColor(ss2);
    REQUIRE(c2.getRed() == 128);
    REQUIRE(c2.getGreen() == 64);
    REQUIRE(c2.getBlue() == 32);

    try {
        Color c3 = parseColor(ss3);
    }
    catch(const DrawerException &ex) {
        std::string errmsg = "Bad stream";
        REQUIRE(ex.what() == errmsg);
    }
}

TEST_CASE("Parsing shapes", "Parser") {
    std::stringstream sstream("Rectangle(10, 20) [50, 100] {green}\n"
                              "Circle(8) [150, 10] {127, 127, 255}\n"
                              "Triangle(10, 10, 10, 20) [250, 400] {255, 0, 0}");

    class Rectangle r(10, 20, 50, 100, Color("green"));
    Circle c(8, 150, 10, Color(127, 127, 255));
    Triangle t(10, 10, 10, 20, 250, 400, Color(255, 0, 0));

    std::vector<std::unique_ptr<Shape>> shapes = parseShapes(sstream);
    REQUIRE(*shapes[0] == r);
    REQUIRE(*shapes[1] == c);
    REQUIRE(*shapes[2] == t);
}

TEST_CASE("Drawing shapes and saving image") {
    //test is passed if program didn't crash :)

    class Rectangle r(10, 20, 50, 100, Color("green"));
    Circle c(8, 150, 10, Color(127, 127, 255));
    Triangle t(10, 10, 10, 20, 250, 400, Color(255, 0, 0));

    Drawer draw(800, 800);
    draw.drawShape(r);
    draw.drawShape(c);
    draw.drawShape(t);

    draw.saveImage();
}