#include <iostream>

#include "parse.hpp"

#include "Drawer.h"

int main(int argc, char** argv) {
    std::pair <unsigned, unsigned> canvasSize;
    std::vector<std::unique_ptr<Shape>> shapes;

    try {
        getInput(argc, argv, canvasSize, shapes);
    }
    catch (const DrawerException &ex) {
        std::cout << "An error occurred! Error: " << ex.what() << ".\n";
        exit(1);
    }

    Drawer draw(canvasSize.first, canvasSize.second);
    for (const auto &s : shapes)
        draw.drawShape(*s);

    draw.saveImage();

    return 0;
}