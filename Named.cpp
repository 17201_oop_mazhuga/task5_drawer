#include <utility>

//
// Created by d.mazhuga on 19.11.2018.
//

#include "Named.h"

Named::Named(std::string str) {
    name = generateName(std::move(str));
}

std::string Named::getName() const {
    return name;
}

std::map<std::string, unsigned> Named::createMap() {
    return std::map<std::string, unsigned int>();
}

std::string Named::generateName(std::string str) {
    namedMap[str]++;
    return str + "." + std::to_string(namedMap[str]);
}

std::map <std::string, unsigned> Named::namedMap = Named::createMap();

std::ostream &operator<<(std::ostream &out, const Named &nmd) {
    out << nmd.getName();
    return out;
}
