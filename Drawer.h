//
// Created by d.mazhuga on 26.11.2018.
//

#ifndef INC_05_DRAWER_DRAWER_H
#define INC_05_DRAWER_DRAWER_H

#include "bitmap/bitmap_image.hpp"
#include "Shape.h"

class Drawer {
private:
    bitmap_image img;
    image_drawer drw;
public:
    Drawer(unsigned imageWidth, unsigned imageHeight);
    void drawShape(const Shape &sh);
    void saveImage(std::string name = "noname.bmp");
};


#endif //INC_05_DRAWER_DRAWER_H
