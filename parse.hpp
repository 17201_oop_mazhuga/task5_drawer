//
// Created by 214 on 04.12.2018.
//

#include <vector>
#include <fstream>
#include <memory>
#include <climits>

#include "DrawerException.h"
#include "Shape.h"

std::pair<unsigned, unsigned> parseCanvasSize(std::istream &inputStream) {
    std::string str;
    char *strPointer;
    unsigned width, height;

    if (!(getline(inputStream, str, '\n')))
        throw DrawerException("Bad stream");

    width = std::strtoul(str.data(), &strPointer, 10);
    height = std::strtoul(strPointer + 1, nullptr, 10);

    if (errno == ERANGE || width == 0 || height == 0 || width == ULONG_MAX || height == ULONG_MAX)
        throw DrawerException("Wrong input format for canvas size");

    return std::make_pair(width, height);

}

std::pair<unsigned, unsigned> parseCoordinates(std::istream &inputStream) {
    unsigned x, y;

    std::string str;
    char *strPointer;

    if (!(getline(inputStream, str, '[')))
        throw DrawerException("Bad stream");
    if (!(getline(inputStream, str, ']')))
        throw DrawerException("Bad stream");

    x = std::strtoul(str.data(), &strPointer, 10);
    y = std::strtoul(strPointer + 2, nullptr, 10);

    if (errno == ERANGE || x == ULONG_MAX || y == ULONG_MAX)
        throw DrawerException("Wrong input format for coordinates size");

    return std::make_pair(x, y);
}

Color parseColor(std::istream &inputStream) {
    Color color;
    unsigned r, g, b;

    std::string str;
    char *strPointer;

    if (!(getline(inputStream, str, '{')))
        throw DrawerException("Bad stream");
    if (!(getline(inputStream, str, '}')))
        throw DrawerException("Bad stream");

    try {
        color = Color(str);
    }
    catch (DrawerException) {
        r = std::strtoul(str.data(), &strPointer, 10);
        g = std::strtoul(strPointer + 2, &strPointer, 10);
        b = std::strtoul(strPointer + 2, nullptr, 10);

        if (errno == ERANGE || r > UCHAR_MAX || g > UCHAR_MAX || b > UCHAR_MAX)
            throw DrawerException("Wrong input format for color");
        
        color = Color((unsigned char)r, (unsigned char)g, (unsigned char)b);
    }

    return color;
}

std::vector<std::unique_ptr<Shape>> parseShapes(std::istream &inputStream) {
    std::vector<std::unique_ptr<Shape>> shapes;
    std::string str, shapeName;

    while (getline(inputStream, shapeName, '(')) {
        std::pair<unsigned, unsigned> coordinates;
        Color color;

        if (shapeName == "Rectangle") {
            unsigned width, height;

            if (!(getline(inputStream, str, ')')))
                throw DrawerException("Bad stream");

            char *strPointer;

            width = std::strtoul(str.data(), &strPointer, 10);
            height = std::strtoul(strPointer + 1, nullptr, 10);

            if (errno == ERANGE || width == 0 || height == 0 || width == ULONG_MAX || height == ULONG_MAX)
                throw DrawerException("Wrong input format for rectangle parameters");


            coordinates = parseCoordinates(inputStream);
            color = parseColor(inputStream);
            inputStream.get();

            //auto s = std::make_unique<class Rectangle>(width, height, coordinates.first, coordinates.second, color);
            shapes.emplace_back(std::make_unique<class Rectangle>(width, height, coordinates.first, coordinates.second, color));

        } else if (shapeName == "Triangle") {
            unsigned x1, y1, x2, y2;

            if (!(getline(inputStream, str, ')')))
                throw DrawerException("Bad stream");

            char *strPointer;

            x1 = std::strtoul(str.data(), &strPointer, 10);
            y1 = std::strtoul(strPointer + 1, &strPointer, 10);
            x2 = std::strtoul(strPointer + 1, &strPointer, 10);
            y2 = std::strtoul(strPointer + 1, &strPointer, 10);

            if (errno == ERANGE || x1 == 0 || y1 == 0 || x2 == 0 || y2 == 0 ||
                x1 == ULONG_MAX || y1 == ULONG_MAX || x2 == ULONG_MAX || y2 == ULONG_MAX)
                throw DrawerException("Wrong input format for triangle parameters");
            
            coordinates = parseCoordinates(inputStream);
            color = parseColor(inputStream);
            inputStream.get();

            //auto s = std::make_unique<Triangle>(x1, y1, x2, y2, coordinates.first, coordinates.second, color);
            shapes.emplace_back(std::make_unique<Triangle>(x1, y1, x2, y2, coordinates.first, coordinates.second, color));

        } else if (shapeName == "Circle") {
            unsigned radius;

            if (!(getline(inputStream, str, ')')))
                throw DrawerException("Bad stream");

            radius = std::strtoul(str.data(), nullptr, 10);

            if (errno == ERANGE || radius == 0 || radius == ULONG_MAX)
                throw DrawerException("Wrong input format for circle parameters");
            
            coordinates = parseCoordinates(inputStream);
            color = parseColor(inputStream);
            inputStream.get();

            //auto s = std::make_unique<Circle>(radius, coordinates.first, coordinates.second, color);
            shapes.emplace_back(std::make_unique<Circle>(radius, coordinates.first, coordinates.second, color));

        } else {
            throw DrawerException("Wrong shape name (" + shapeName + ')');
        }
    }

    return shapes;
}

void getInput(int argc, char **argv, std::pair<unsigned, unsigned> &canvasSize,
              std::vector<std::unique_ptr<Shape>> &shapes) {
    std::string fileName;
    if (argc > 1)
        fileName = argv[1];
    else
        throw DrawerException("Not enough arguments");

    if (fileName == "-") {
        canvasSize = parseCanvasSize(std::cin);
        shapes = parseShapes(std::cin);
    } else {
        std::ifstream fileStream;
        fileStream.open(fileName);
        canvasSize = parseCanvasSize(fileStream);
        shapes = parseShapes(fileStream);
    }
}