//
// Created by d.mazhuga on 19.11.2018.
//

#ifndef INC_05_DRAWER_COLORED_H
#define INC_05_DRAWER_COLORED_H

#include "Color.h"

class Colored {
protected:
    Color color;
public:
    virtual ~Colored(){};
    Color getColor() const;
    void setColor(const Color &c);
};


#endif //INC_05_DRAWER_COLORED_H
