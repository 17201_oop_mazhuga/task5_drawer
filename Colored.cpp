//
// Created by d.mazhuga on 19.11.2018.
//

#include "Colored.h"

Color Colored::getColor() const {
    return color;
}

void Colored::setColor(const Color &c) {
    color = c;
}
