//
// Created by 214 on 25.11.2018.
//

#ifndef INC_05_DRAWER_SEGMENT_H
#define INC_05_DRAWER_SEGMENT_H

#include <vector>

class Segment {
public:
    unsigned x0, y0, x1, y1;

    Segment(unsigned int arg_x0, unsigned int arg_y0, unsigned int arg_x1, unsigned int arg_y1) : x0(arg_x0),
                                                                                                  y0(arg_y0),
                                                                                                  x1(arg_x1),
                                                                                                  y1(arg_y1) {};

    bool operator==(const Segment &other) const {
        return (x0 == other.x0 && y0 == other.y0 && x1 == other.x1 && y1 == other.y1) ||
               (x0 == other.x1 && y0 == other.y1 && x1 == other.x0 && y1 == other.y0);
        //symmetrical segments are equal
    }
};


#endif //INC_05_DRAWER_SEGMENT_H
