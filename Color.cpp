//
// Created by d.mazhuga on 19.11.2018.
//

#include "Color.h"

Color::Color() : r(0), g(0), b(0) {
}

Color::Color(unsigned char rc, unsigned char gc, unsigned char bc) : r(rc), g(gc), b(bc) {
}

Color::Color(std::string colorName) {
    if (colorName == "black") {
        r = 0;
        g = 0;
        b = 0;
    } else if (colorName == "white") {
        r = 255;
        g = 255;
        b = 255;
    } else if (colorName == "red") {
        r = 255;
        g = 0;
        b = 0;
    } else if (colorName == "green") {
        r = 0;
        g = 255;
        b = 0;
    } else if (colorName == "blue") {
        r = 0;
        g = 0;
        b = 255;
    } else if (colorName == "grey") {
        r = 128;
        g = 128;
        b = 128;
    } else if (colorName == "yellow") {
        r = 255;
        g = 255;
        b = 0;
    } else if (colorName == "cyan") {
        r = 0;
        g = 255;
        b = 255;
    } else if (colorName == "magenta") {
        r = 255;
        g = 0;
        b = 255;
    } else {
        throw DrawerException("No color matching given name");
    }

}

unsigned char Color::getRed() const {
    return r;
}

unsigned char Color::getGreen() const {
    return g;
}

unsigned char Color::getBlue() const {
    return b;
}
