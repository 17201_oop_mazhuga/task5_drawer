//
// Created by d.mazhuga on 26.11.2018.
//

#include "Drawer.h"

Drawer::Drawer(unsigned imageWidth, unsigned imageHeight) : img(imageWidth, imageHeight), drw(img) {
    img.set_all_channels(255, 255, 255);
    drw.pen_width(1);
    drw.pen_color(0, 0, 0);
}

void Drawer::drawShape(const Shape &sh) {
    std::vector<Segment> segments = sh.getSegments();
    Color color = sh.getColor();
    drw.pen_color(color.getRed(), color.getGreen(), color.getBlue());
    for (const auto &s : segments)
        drw.line_segment(s.x0, s.y0, s.x1, s.y1);
}

void Drawer::saveImage(std::string name) {
    img.save_image(name);
}
