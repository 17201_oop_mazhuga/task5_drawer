//
// Created by d.mazhuga on 19.11.2018.
//

#ifndef INC_05_DRAWER_SHAPE_H
#define INC_05_DRAWER_SHAPE_H

#include <iostream>
#include <typeinfo>
#include <cmath>
#include <cstdarg>

#include "Named.h"
#include "Colored.h"
#include "Segment.h"

class Shape : public Named, public Colored {
protected:
    unsigned xCenter;
    unsigned yCenter;

    virtual bool equal(const Shape &other) const = 0;
public:
    virtual ~Shape(){};
    void setCentralCoordinates(unsigned x0, unsigned y0);
    virtual std::vector<Segment> getSegments() const = 0;
    virtual double square() const = 0;

    bool operator==(const Shape &other) const;
};


class Rectangle : virtual public Shape {
private:
    unsigned width, height;
    bool equal(const Shape &other) const override;
public:
    Rectangle(unsigned width, unsigned height, unsigned xc = 0, unsigned yc = 0,
              Color clr = static_cast<std::string>("black"));

    void setParameters(unsigned w, unsigned h);
    std::vector<Segment> getSegments() const override;
    double square() const override;
};


class Triangle : public Shape{
private:
    unsigned x1, y1, x2, y2;
    bool equal(const Shape &other) const override;
public:
    Triangle(unsigned x1, unsigned y1, unsigned x2, unsigned y2, unsigned xc = 0, unsigned yc = 0,
             Color clr = static_cast<std::string>("black"));
    void setCoordinates(unsigned x1_a, unsigned y1_a, unsigned x2_a, unsigned y2_a);
    std::vector<Segment> getSegments() const override;
    double square() const override;
};


class Circle : public Shape{
private:
    unsigned radius;
    bool equal(const Shape &other) const override;
public:
    explicit Circle (unsigned radius, unsigned xc = 0, unsigned yc = 0,
                     Color clr = static_cast<std::string>("black"));
    void setRadius(unsigned rad);
    std::vector<Segment> getSegments() const override;
    double square() const override;
};


template<typename T>
void printNames(const T &value) {
    std::cout << value << std::endl;
}

template<typename T, typename ... Args>
void printNames(const T &value, const Args... args) {
    std::cout << value << ' ';
    printNames(args...);
}

template<typename T>
double totalArea(const T &value) {
    return value.square();
}

template<typename T, typename ... Args>
double totalArea(const T &value, Args... args) {
    return totalArea(value) + totalArea(args...);
}

#endif //INC_05_DRAWER_SHAPE_H
