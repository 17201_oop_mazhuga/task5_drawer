//
// Created by d.mazhuga on 19.11.2018.
//

#ifndef INC_05_DRAWER_COLOR_H
#define INC_05_DRAWER_COLOR_H

#include <string>
#include "DrawerException.h"

class Color {
private:
    unsigned char r, g, b;
public:
    Color();
    Color(unsigned char rc, unsigned char gc, unsigned char bc);
    Color(std::string colorName);

    unsigned char getRed() const;
    unsigned char getGreen() const;
    unsigned char getBlue() const;
};


#endif //INC_05_DRAWER_COLOR_H
