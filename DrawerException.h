#include <utility>

//
// Created by 214 on 07.12.2018.
//

#ifndef INC_05_DRAWER_MYEXCEPTION_H
#define INC_05_DRAWER_MYEXCEPTION_H

#include <exception>
#include <string>

class DrawerException : public std::exception {
private:
    std::string str;
public:
    explicit DrawerException(std::string s) : str(std::move(s)) {}

    const char *what() const noexcept override{
        return str.data();
    };
};

#endif //INC_05_DRAWER_MYEXCEPTION_H
